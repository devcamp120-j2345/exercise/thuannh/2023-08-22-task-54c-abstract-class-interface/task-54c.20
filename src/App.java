import models.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point1 = new MovablePoint(5, 6);
        MovablePoint point2 = new MovablePoint(5, 6);

        System.out.println(point1.toString());
        System.out.println(point2.toString());

        point1.moveUp();
        point1.moveUp();
        point1.moveLeft();

        System.out.println(point1.toString());

        point2.moveDown();
        point2.moveDown();
        point2.moveRight();

        System.out.println(point2.toString());
    }
}
